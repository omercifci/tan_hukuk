import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import Header from "../../components/Header/Header.js";
import Footer from "../../components/Footer/Footer.js";
import HeaderLinks from "../../components/Header/HeaderLinks.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
import styles from "../../assets/jss/nextjs-material-kit/pages/landingPage.js";

import tan_paper_black from "../../assets/img/tan-paper-black.jpeg";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function Tuketiciukuku(props) {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRounded,
    classes.imgFluid,
    classes.imgGallery
  );
  const containerClasses = classNames(classes.containerSection, classes.margin);
  const { ...rest } = props;

  return (
    <div>
      <Header
        color="dark"
        routes={dashboardRoutes}
        brand="TAN HUKUK & DANIŞMANLIK"
        rightLinks={<HeaderLinks />}
        fixed
        {...rest}
      />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.containerSection}>
          <div className={classes.section}>
            <GridContainer justify="center">
              <GridItem xs={10} sm={8} md={6} className={classes.verticalImage}>
                <img
                  src={tan_paper_black}
                  alt="tan_hukuk_logo"
                  className={imageClasses}
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={8}>
                <h2 className={classes.sectionTitle}>Tüketici Hukuku</h2>
                <h4 className={classes.description}>
                  Tüketici hukuku, hukuk dünyası gucsuzu gucluye karşı
                  korumaktadır. Bu açıdan tüketici korunmakta, hak ve
                  menfaatleri güvence altına alınmaktadır. Büromuz bu hak ve
                  menfaatleri, tüketici lehine korunmasını sağlayarak
                  uyuşmazlıkları çizmektedir.
                </h4>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
