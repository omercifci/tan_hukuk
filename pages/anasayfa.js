import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";

import styles from "assets/jss/nextjs-material-kit/pages/landingPage.js";

// Sections for this page
import ServicesSection from "pages-sections/LandingPage-Sections/ServicesSection.js";
import TeamSection from "pages-sections/LandingPage-Sections/TeamSection.js";
import ContactSection from "pages-sections/LandingPage-Sections/ContactSection.js";
import AboutSection from "pages-sections/LandingPage-Sections/AboutSection.js";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="TAN HUKUK & DANIŞMANLIK"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "dark",
        }}
        {...rest}
      />
      <Parallax filter responsive image={require("assets/img/bg_law.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={8} className={classes.itemGrid}>
              <h3 className={classes.title}>
                GÜVENİLİR VE ETKİLİ HUKUKİ DANIŞMANLIK HİZMETİ
              </h3>
              <h5>
                Tan Hukuk & Danışmanlık, Ankara merkezli bir hukuk bürosu olup
                tüm Türkiye genelinde müvekkillerine hızlı ve etkin bir şekilde
                avukatlık ve hukuki danışmanlık hizmeti vermektedir. Faaliyet
                gösterdiği çalışma alanlarında müvekkillerinin hukuki
                sorunlarının verimli ve doğru şekilde çözümlenmesini
                amaçlamaktadır. Hukuki işlem ve uyuşmazlıklarınızın çözümü
                kapsamında büromuzca, güncel içtihatlar ve mevzuat ışığında
                süratli ve şeffaf bir hukuki destek sağlanmaktadır.
              </h5>
              <br />
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <AboutSection />
          <ServicesSection />
          <TeamSection />
          <ContactSection />
        </div>
      </div>
      <Footer />
    </div>
  );
}
