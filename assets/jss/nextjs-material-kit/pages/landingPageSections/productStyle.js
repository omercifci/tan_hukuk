import { successColor, title } from "assets/jss/nextjs-material-kit.js";

const productStyle = {
  section: {
    padding: "70px 0",
    textAlign: "center",
  },
  bgDark: {
    padding: "70px 0",
    textAlign: "center",
    background: "#172d48",
  },
  title: {
    ...title,
    marginBottom: "1rem",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none",
  },
  description: {
    color: "#999",
  },
  logo: {
    width: "50%",
    height: "auto",
  },
};

export default productStyle;
