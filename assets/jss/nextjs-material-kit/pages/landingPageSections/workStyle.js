import { title } from "assets/jss/nextjs-material-kit.js";
import imagesStyle from "assets/jss/nextjs-material-kit/imagesStyles.js";
import { primaryColor, successColor } from "assets/jss/nextjs-material-kit.js";

const workStyle = {
  section: {
    padding: "70px 0",
    textAlign: "center",
  },
  sectionBorder: {
    padding: "70px 20px 50px 20px",
    borderStyle: "solid",
    borderWidth: "2px",
    borderColor: primaryColor,
    marginBottom: "50px",
  },
  title: {
    ...title,
    marginBottom: "50px",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none",
    textAlign: "center",
  },
  ...imagesStyle,
  description: {
    color: "#999",
    textAlign: "center",
  },
  textCenter: {
    textAlign: "center",
  },
  textArea: {
    marginRight: "15px",
    marginLeft: "15px",
  },
  largeIcon: {
    width: "100%",
    height: 100,
    color: successColor,
  },
  primaryColor: {
    primaryColor,
  },
  successColor: {
    successColor,
  },
};

export default workStyle;
