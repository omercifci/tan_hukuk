/*eslint-disable*/
import React from "react";
import Link from "next/link";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Apps, BusinessCenter, ContactPhone } from "@material-ui/icons";
import PeopleIcon from "@material-ui/icons/People";
import PhoneIcon from "@material-ui/icons/Phone";
import BorderColorIcon from "@material-ui/icons/BorderColor";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";

// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/nextjs-material-kit/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Button
          href="/hakkimizda"
          color="transparent"
          className={classes.navLink}
        >
          <BorderColorIcon></BorderColorIcon> Hakkımızda
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          navDropdown
          buttonText="Çalışma Alanlarımız"
          buttonProps={{
            className: classes.navLink,
            color: "transparent",
          }}
          hoverColor="black"
          buttonIcon={Apps}
          dropdownList={[
            <Link href="/hizmetlerimiz/cezahukuku">
              <a className={classes.dropdownLink}>Ceza Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/idarehukuku">
              <a className={classes.dropdownLink}>İdare Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/ishukuku">
              <a className={classes.dropdownLink}>İş Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/ticarethukuku">
              <a className={classes.dropdownLink}>Ticaret Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/icraiflashukuku">
              <a className={classes.dropdownLink}>İcra ve İflas Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/vergihukuku">
              <a className={classes.dropdownLink}>Vergi Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/ailehukuku">
              <a className={classes.dropdownLink}>Aile Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/bilisimhukuku">
              <a className={classes.dropdownLink}>Bilişim Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/rekabethukuku">
              <a className={classes.dropdownLink}>Rekabet Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/enerjihukuku">
              <a className={classes.dropdownLink}>Enerji Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/mirashukuku">
              <a className={classes.dropdownLink}>Miras Hukuku</a>
            </Link>,
            <Link href="/hizmetlerimiz/tuketicihukuku">
              <a className={classes.dropdownLink}>Tüketici Hukuku</a>
            </Link>,
          ]}
        />
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="/ekibimiz"
          color="transparent"
          className={classes.navLink}
        >
          <PeopleIcon></PeopleIcon> Kadromuz
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button href="/kariyer" color="transparent" className={classes.navLink}>
          <BusinessCenterIcon></BusinessCenterIcon> Kariyer
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="/iletisim"
          color="transparent"
          className={classes.navLink}
        >
          <PhoneIcon></PhoneIcon> İletişim
        </Button>
      </ListItem>

      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-tooltip"
          title="Bizi Instagram'da Takip Et"
          placement={"top"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.instagram.com/tanhukukdanismanlik/"
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-instagram"} />
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="linkedin-tooltip"
          title="Bizi LinkedIn'de Takip Et"
          placement={"top"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.linkedin.com/company/tanhukukdan%C4%B1%C5%9Fmanl%C4%B1k/"
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-linkedin"} />
          </Button>
        </Tooltip>
      </ListItem>
    </List>
  );
}
