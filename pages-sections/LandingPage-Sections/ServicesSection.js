import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import MonetizationOnOutlinedIcon from "@material-ui/icons/MonetizationOnOutlined";
import Fingerprint from "@material-ui/icons/Fingerprint";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import BorderColorIcon from "@material-ui/icons/BorderColor";
import BusinessIcon from "@material-ui/icons/Business";
import GavelIcon from "@material-ui/icons/Gavel";
import MarkunreadIcon from "@material-ui/icons/Markunread";
import HomeWorkIcon from "@material-ui/icons/HomeWork";
import ReceiptIcon from "@material-ui/icons/Receipt";
import FlashOnIcon from "@material-ui/icons/FlashOn";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/nextjs-material-kit/pages/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ServicesSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h3 className={classes.title}>ÇALIŞMA ALANLARIMIZ</h3>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Ceza Hukuku"
              description="Ceza Hukuku, gerek mağdurun gerek sanığın korunmasını garanti eden, kişilere kanuni güvenceler ve haklar tanıyan hukuk alanıdır..."
              icon={GavelIcon}
              iconColor="primary"
              vertical
            />
            <Button
              href="/hizmetlerimiz/cezahukuku"
              size="sm"
              simple
              color="primary"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="İdare Hukuku"
              description="İdare Hukuku, kişinin Devlet ile olan ilişkisinde ortaya çıkması muhtemel aksaklıkları gidermeyi amaçlayan ve idari ilişkileri düzenleyen hukuk alanıdır..."
              icon={BorderColorIcon}
              iconColor="success"
              vertical
            />
            <Button
              href="/hizmetlerimiz/idarehukuku"
              size="sm"
              simple
              color="success"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="İş Hukuku"
              description="İş Hukuku ülkemiz ekonomisinin büyük kısmını oluşturan ilişkilerin düzenlendiği alandır. Bireysel ya da toplu iş ve hizmet sözleşmelerinin..."
              icon={BusinessIcon}
              iconColor="primary"
              vertical
            />
            <Button
              href="/hizmetlerimiz/ishukuku"
              size="sm"
              simple
              color="primary"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Ticaret Hukuku"
              description="Kişiler arasındaki ticari faaliyetleri ve ticaretle uğraşanlar arasındaki ilişkileri düzenleyen hukuk dalıdır. Yalnızca ticaretle uğraşan kimselerin..."
              icon={MonetizationOnOutlinedIcon}
              iconColor="success"
              vertical
            />
            <Button
              href="/hizmetlerimiz/ticarethukuku"
              size="sm"
              simple
              color="success"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="İcra ve İflas Hukuku"
              description="Tan Hukuk ve Danışmanlık icra ve iflas hukuku alanında, alacağın tahsili amaçlı borçluya ait tüm gayrimenkul, menkul ,3. kişideki alacak ve taşınırlarına... "
              icon={AccountBalanceIcon}
              iconColor="primary"
              vertical
            />
            <Button
              href="/hizmetlerimiz/icraiflashukuku"
              size="sm"
              simple
              color="primary"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Vergi Hukuku"
              description="Vergi hukuku, kamu hukuku içerisinde yer alan ve devletin mali faaliyetlerinin hukuki yönünü inceleyen mali hukukun bir alt dalıdır..."
              icon={ReceiptIcon}
              iconColor="success"
              vertical
            />
            <Button
              href="/hizmetlerimiz/vergihukuku"
              size="sm"
              simple
              color="success"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Aile Hukuku"
              description="Aile ile ilgili olan tüm konuları içerisinde barındıran, aile bireylerinin ve özellikle eşlerin birbirine karşı hak ve yükümlülüklerini düzenleyen..."
              icon={HomeOutlinedIcon}
              iconColor="primary"
              vertical
            />
            <Button
              href="/hizmetlerimiz/ailehukuku"
              size="sm"
              simple
              color="primary"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Bilişim Hukuku"
              description="Bilişim hukuku temel olarak teknoloji ve bilişim sistemlerinin birlikte kullanılması ile üretilen sonuçlara bağlanan hukuki çözüm yollarına ilişkin..."
              icon={Fingerprint}
              iconColor="success"
              vertical
            />
            <Button
              href="/hizmetlerimiz/bilisimhukuku"
              size="sm"
              simple
              color="success"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Rekabet Hukuku"
              description="Rekabet hukuku, serbest piyasa ekonomisinde tekelleşmenin önlenmesi ve ekonomik etkinliğin sağlanması düşüncesinden doğmuş bir hukuk alanıdır... "
              icon={HomeWorkIcon}
              iconColor="primary"
              vertical
            />
            <Button
              href="/hizmetlerimiz/rekabethukuku"
              size="sm"
              simple
              color="primary"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Enerji Hukuku"
              description="Enerji hukuku, günümüz dünyasında önemi ve etki sahası gittikçe artan enerji piyasası aktörlerinin uyması gereken kuralların sistematize edildiği hukuk alanıdır..."
              icon={FlashOnIcon}
              iconColor="success"
              vertical
            />
            <Button
              href="/hizmetlerimiz/enerjihukuku"
              size="sm"
              simple
              color="success"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Miras Hukuku"
              description="Miras hukuku, gerçek kişinin ölümü veya gaipliği halinde, bu kişinin malvarlığının kanunun öngördüğü şekilde, kimlere ve nasıl intikal edeceğini düzenleyen hukuk dalıdır..."
              icon={MarkunreadIcon}
              iconColor="primary"
              vertical
            />
            <Button
              href="/hizmetlerimiz/mirashukuku"
              size="sm"
              simple
              color="primary"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <InfoArea
              title="Tüketici Hukuku"
              description="Tüketici hukuku, hukuk dünyası gucsuzu gucluye karşı korumaktadır. Bu açıdan tüketici korunmakta, hak ve menfaatleri güvence altına alınmaktadır. Büromuz bu hak ve menfaatleri..."
              icon={ShoppingBasketIcon}
              iconColor="success"
              vertical
            />
            <Button
              href="/hizmetlerimiz/tuketicihukuku"
              size="sm"
              simple
              color="success"
            >
              Devamını Oku <ArrowForwardIosIcon />
            </Button>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
