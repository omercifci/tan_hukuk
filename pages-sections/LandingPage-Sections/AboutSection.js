import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import styles from "assets/jss/nextjs-material-kit/pages/landingPageSections/productStyle.js";
import logo from "../../assets/img/logo-tanhukuk.png";

const useStyles = makeStyles(styles);

export default function AboutSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8} className={classes.itemGrid}>
          <img src={logo} alt="tan_hukuk_logo" className={classes.logo} />
        </GridItem>
        <GridItem xs={12} sm={12} md={8}>
          <h5 className={classes.description}>
            Tan Hukuk & Danışmanlık başta Ticaret Hukuku, İş Hukuku, Ceza
            Hukuku, İdare Hukuku, Miras Hukuku, Aile Hukuku, İcra ve İflas.,
            Vergi Hukuku, Tüketici Hukuku gibi birçok alanda, hukuki
            uyuşmazlıkların çözümlenmesine yönelik faaliyetler yürütmekte ve
            hizmet vermektedir.
          </h5>
        </GridItem>
      </GridContainer>
    </div>
  );
}
