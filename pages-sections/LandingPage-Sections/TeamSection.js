import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "assets/jss/nextjs-material-kit/pages/landingPageSections/teamStyle.js";

import img_vedat from "assets/img/faces/vedat_erdem.jpeg";
import img_furkan from "assets/img/faces/furkan_er.jpeg";
import img_umut from "assets/img/faces/umut_ozgur.jpeg";
import img_serkan from "assets/img/faces/serkan_kuce.jpeg";
import img_reha from "assets/img/faces/reha_inanc.jpeg";
import PhoneIcon from "@material-ui/icons/Phone";


const useStyles = makeStyles(styles);

export default function TeamSection() {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRounded,
    classes.imgFluid
  );
  return (
    <div className={classes.section}>
      <h3 className={classes.title}>AVUKAT KADROMUZ</h3>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={img_reha} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Av. Reha İNANÇ
                <br />
                <small className={classes.smallTitle}>Kurucu Ortak</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                Ankara Üniversitesi Hukuk Fakültesi’nden mezun oldu. 
                Malta'da Uluslararası Deniz Ticareti ve Avrupa Birliği 
                Sigorta Hukuku alanlarında staj yaptı. Türkiye'ye döndükten 
                sonra yasal stajının ardından 
                Tan Hukuk & Danışmanlık kurucu üyeleri arasında yerini aldı.
                </p>
                <br />
                <p className={classes.description}>
                  <b>Yabancı Dil: </b>İngilizce
                </p>
                <p className={classes.description}>
                  <b>Mail: </b> av.rehainanc@gmail.com
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  href="https://www.linkedin.com/in/rehainanc"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
                <Button
                  href="tel:+90-312-999-7929"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <PhoneIcon/>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={img_furkan} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Av. Furkan ER
                <br />
                <small className={classes.smallTitle}>Kurucu Ortak</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Ankara Üniversitesi Hukuk Fakültesi’nden mezun oldu. Yasal
                  stajının ardından Tan Hukuk & Danışmanlık kurucu üyeleri
                  arasında yerini aldı. Hâlihazırda Hacettepe Ünivesitesi Özel
                  Hukuk Anabilim Dalı’nda yüksek lisans yapmaktadır. Ayrıca iyi
                  derecede İngilizce bilmektedir.
                </p>
                <p className={classes.description}>
                  <b>Yabancı Dil: </b>İngilizce
                </p>
                <p className={classes.description}>
                  <b>Mail: </b> avukatfurkaner@gmail.com
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  href="https://www.linkedin.com/in/avfurkaner"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
                <Button
                  href="tel:+90-553-207-3108"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <PhoneIcon/>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={img_vedat} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Av. Vedat ERDEM
                <br />
                <small className={classes.smallTitle}>Kurucu Ortak</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Ankara Üniversitesi Hukuk Fakültesinden mezun oldu. Yasal
                  stajının ardından Tan Hukuk ve Danışmanlık Ofisi kurucu
                  üyeleri arasında yer aldı. Halihazırda Ankara Sosyal Bilimler
                  Üniversitesinde Bilişim ve Teknoloji Hukuku alanında yüksek
                  lisans yapmaktadır. Ayrıca iyi derecede İngilizce ve Almanca
                  bilmektedir.
                </p>
                <p className={classes.description}>
                  <b>Yabancı Dil: </b>İngilizce, Almanca
                </p>
                <p className={classes.description}>
                  <b>Mail: </b> av.erdemvedat@gmail.com
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  href="https://www.linkedin.com/in/avukatvedaterdem/"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
                <Button
                  href="tel:+90-544-494-1743"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <PhoneIcon/>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={6}>
            <Card plain>
              <GridItem xs={12} sm={12} md={4} className={classes.itemGrid}>
                <img src={img_umut} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Av. Umut Ömür Can ÖZGÜR
                <br />
                <small className={classes.smallTitle}>Kurucu Ortak</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Ankara Üniversitesi Hukuk Fakültesi mezuniyetinin ardından,
                  avukatlık stajını verimli bir şekilde yaparak tecrübe kazanan
                  Özgür; Tan Hukuk & Danışmanlık Bürosu kurucuları arasında
                  yerini almıştır. Hacettepe Üniversitesi Özel Hukuk anabilim
                  dalında yüksek lisans eğitimini sürdürmekte olup, iyi derecede
                  İngilizce bilmektedir.
                </p>
                <p className={classes.description}>
                  <b>Yabancı Dil: </b>İngilizce
                </p>
                <p className={classes.description}>
                  <b>Mail: </b> av.umutomurcanozgur@gmail.com
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  href="https://www.linkedin.com/in/umutomurcanozgur/ "
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
                <Button
                  href="tel:+90-534-624-6137"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <PhoneIcon/>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={6}>
            <Card plain>
              <GridItem xs={12} sm={12} md={4} className={classes.itemGrid}>
                <img src={img_serkan} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Av. Serkan KÜCE
                <br />
                <small className={classes.smallTitle}>Kurucu Ortak</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Lisans eğitimini Ankara Üniversitesi Hukuk Fakültesinde
                  tamamladı. Staj dönemi çalışmasının yanı sıra anonim
                  şirketlerin halka arzı ile yurtdışı işçilik alacakları
                  konularıyla ilgilendi. Yasal stajının ardından Tan Hukuk &
                  Danışmanlık kurucu üyeleri arasında yerini aldı.
                </p>
                <br />
                <p className={classes.description}>
                  <b>Yabancı Dil: </b>İngilizce
                </p>
                <p className={classes.description}>
                  <b>Mail: </b> av.serkankuce@gmail.com
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  href="https://tr.linkedin.com/in/serkankuce"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
                <Button
                  href="tel:+90-531-584-2886"
                  target="_blank"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <PhoneIcon/>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
