import React from "react";

// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import PhoneEnabledOutlinedIcon from "@material-ui/icons/PhoneEnabledOutlined";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import RoomOutlinedIcon from "@material-ui/icons/RoomOutlined";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/nextjs-material-kit/pages/landingPageSections/workStyle.js";
import Card from "../../components/Card/Card";
import Iframe from "react-iframe";
import { Link } from "@material-ui/core";

import tan_office from "../../assets/img/tan-office.jpeg";

const useStyles = makeStyles(styles);

export default function ContactSection() {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgFluid,
    classes.imgRaised,
    classes.imgRoundedCircle
  );
  return (
    <div className={classes.section}>
      <h3 className={classes.title}>İLETİŞİM</h3>
      <div>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8} className={classes.itemGrid}>
            <img
              src={tan_office}
              alt="tan_hukuk_logo"
              className={imageClasses}
            />
          </GridItem>
          <GridItem cs={12} sm={12} md={6}>
            <Card plain>
              <PhoneEnabledOutlinedIcon className={classes.largeIcon} />
              <Button
                target="_blank"
                size="lg"
                fullWidth
                color="transparent"
                className={classes.margin5}
              >
                <Link href="tel:+90-312-999-7929" color="textPrimary">
                  0312 999 79 29
                </Link>
              </Button>
            </Card>
          </GridItem>
          <GridItem cs={12} sm={12} md={6}>
            <Card plain>
              <MailOutlineIcon className={classes.largeIcon} />
              <Button
                target="_blank"
                size="lg"
                fullWidth
                color="transparent"
                className={classes.margin5}
              >
                <Link href="mailto:tanhukukdan@gmail.com" color="textPrimary">
                  tanhukukdan@gmail.com
                </Link>
              </Button>
            </Card>
          </GridItem>
          <GridItem cs={12} sm={12} md={12}>
            <Card plain>
              <RoomOutlinedIcon className={classes.largeIcon} />
              <Button
                target="_blank"
                size="lg"
                fullWidth
                color="transparent"
                className={classes.margin5}
              >
                <Link
                  href="https://goo.gl/maps/f4h1smzegnLVrv3G7"
                  target="_blank"
                  color="textPrimary"
                >
                  Cevizlidere Mahallesi <br />
                  1242. Cadde No: 5/13 <br />
                  06520 Balgat/Ankara
                </Link>
              </Button>
              <div xs={12} sm={12} md={12}>
                <Iframe
                  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4716.726891962842!2d32.816602955190774!3d39.88877106927407!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe0026156e5317caf!2zVEFOIEh1a3VrICYgRGFuxLHFn21hbmzEsWs!5e0!3m2!1str!2str!4v1613304445894!5m2!1str!2str"
                  frameBorder="5px"
                  style="border:0;"
                  aria-hidden="false"
                  tabIndex="0"
                  width="100%"
                  height="250px"
                />
              </div>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
